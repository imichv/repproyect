function buscarPalabra(){
    var iframedoc = aceptado.document;

    if(aceptado.contentDocument){
        iframedoc = aceptado.contentDocument;
    }else{
        if (aceptado.contentWindow){
            iframedoc = aceptado.contentWindow.document;
        }
    }

    if ( document.getElementById('300c').value.length >= 300 )
	{
		if (iframedoc){
            iframedoc.open();
            iframedoc.writeln('ACEPTADO');
            iframedoc.close();

            var parrafo = document.getElementById('300c').value;
            var palabra = document.getElementById('form-palabra').value;
            var pos = parrafo.indexOf(palabra);

            var iframedoc1 = buscador.document;

            if(buscador.contentDocument){
                iframedoc1 = buscador.contentDocument;
            }else{
                if (buscador.contentWindow){
                    iframedoc1 = buscador.contentWindow.document;
                }
            }

            if ( pos!=-1 )
	        {
                iframedoc1.open();
                iframedoc1.writeln('La palabra está en la posición'+' '+pos);
                iframedoc1.close();		
        	}
	        else
	        {
	            iframedoc1.open();
                iframedoc1.writeln('CLAVE NO ENCONTRADA');
                iframedoc1.close();
	        }
        }
        else{
            alert('No es posible insertar el contenido dinámicamente en el iframe.');
        }
	}
	else
	{
	    if (iframedoc){
            iframedoc.open();
            iframedoc.writeln('DENEGADO');
            iframedoc.close();
        }
        else{
            alert('No es posible insertar el contenido dinámicamente en el iframe.');
        }
	}
}